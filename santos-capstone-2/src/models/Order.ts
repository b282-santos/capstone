import { Schema, model } from 'mongoose';
import { Item } from './Cart';

interface OrderSchema {
  userId: Schema.Types.ObjectId;
  name: string;
  address: string;
  phone: string;
  products: Item[];
  totalItems: number;
  totalAmount: number;
  purchasedOn: Date;
}

const orderSchema = new Schema<OrderSchema>(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    products: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true,
        },
        name: {
          type: String,
          required: true,
        },
        price: {
          type: Number,
          required: true,
        },
        quantity: {
          type: Number,
          required: true,
        },
        subTotal: {
          type: Number,
          required: true,
        },
      },
    ],
    totalItems: {
      type: Number,
      required: true,
    },
    totalAmount: {
      type: Number,
      required: true,
    },
    purchasedOn: {
      type: Date,
      default: new Date(),
    },
  },
  { versionKey: false }
);

const Order = model('Order', orderSchema);

export default Order;
