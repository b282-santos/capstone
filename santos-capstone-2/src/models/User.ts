import Joi from 'joi';
import jwt from 'jsonwebtoken';
import { Schema, model } from 'mongoose';

export interface Token {
  _id: string;
  name: string;
  address: string;
  phone: string;
  isAdmin: boolean;
}

interface UserSchema {
  name: string;
  email: string;
  password: string;
  address: string;
  phone: string;
  isAdmin: boolean;
  generateAuthToken(): string;
}

const userSchema = new Schema<UserSchema>(
  {
    name: {
      type: String,
      min: 1,
      max: 50,
      trim: true,
      set: (value: string) => value.replace(/\s+/g, ' '),
      required: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      min: 6,
      max: 1024,
      required: true,
    },
    address: {
      type: String,
      min: 10,
      max: 100,
      trim: true,
      set: (value: string) => value.replace(/\s+/g, ' '),
      required: true,
    },
    phone: {
      type: String,
      min: 11,
      max: 11,
      trim: true,
      set: (value: string) => value.replace(/\s+/g, ''),
      required: true,
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },
  },
  { versionKey: false }
);

userSchema.methods.generateAuthToken = function () {
  return jwt.sign(
    {
      _id: this._id,
      name: this.name,
      address: this.address,
      phone: this.phone,
      isAdmin: this.isAdmin,
    },
    process.env.JWT_SECRET as string
  );
};

const User = model<UserSchema>('User', userSchema);

export default User;

export const validate = (user: UserSchema) => {
  return Joi.object({
    name: Joi.string().min(1).max(50).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(30).required(),
    address: Joi.string().min(10).max(100).required(),
    phone: Joi.string().min(11).max(11).required(),
  }).validate(user);
};
