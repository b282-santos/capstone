// import Joi from 'joi';
import { Schema, model } from 'mongoose';

export interface Item {
  productId: Schema.Types.ObjectId;
  name: string;
  price: number;
  quantity: number;
  subTotal: number;
  gameImage: String;
}

interface CartSchema {
  userId: Schema.Types.ObjectId;
  items: Item[];
  itemsInCart: number;
  totalAmount: number;
  countItemsInCart: () => void;
  computeTotalAmount: () => void;
}

const cartSchema = new Schema<CartSchema>(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true,
        },
        name: {
          type: String,
          required: true,
        },
        price: {
          type: Number,
          required: true,
        },
        quantity: {
          type: Number,
          required: true,
        },
        subTotal: {
          type: Number,
          required: true,
        },
        gameImage: String,
      },
    ],
    itemsInCart: {
      type: Number,
      default: 0,
    },
    totalAmount: {
      type: Number,
      default: 0,
    },
  },
  { versionKey: false }
);

cartSchema.methods.countItemsInCart = function () {
  this.itemsInCart = this.items.length;
};

cartSchema.methods.computeTotalAmount = function () {
  this.totalAmount = this.items.reduce(
    (total: number, item: Item) => total + item.subTotal,
    0
  );
};

const Cart = model('Cart', cartSchema);

export default Cart;

// export const validate = req => {
//   const schema = Joi.object({
//     productId: Joi.required(),
//     quantity: Joi.number().min(1).max(999),
//   });

//   return schema.validate(req);
// };

// export const validateQuantity = req => {
//   const schema = Joi.object({
//     quantity: Joi.number().min(0).max(999).required(),
//   });

//   return schema.validate(req);
// };
