import Joi from 'joi';
import { Schema, model } from 'mongoose';

interface ProductSchema {
  name: string;
  description: string;
  price: number;
  stocks: number;
  gameImage: string;
  isActive: boolean;
  createdOn: Date;
}

const productSchema = new Schema<ProductSchema>(
  {
    name: {
      type: String,
      min: 1,
      max: 255,
      trim: true,
      set: (value: string) => value.replace(/\s+/g, ' '),
      required: true,
    },
    description: {
      type: String,
      min: 1,
      required: true,
    },
    price: {
      type: Number,
      min: 0.01,
      max: 250_000,
      required: true,
    },
    stocks: {
      type: Number,
      min: 0,
      max: 999,
      required: true,
    },
    gameImage: String,
    isActive: {
      type: Boolean,
      default: true,
    },
    createdOn: {
      type: Date,
      default: new Date(),
    },
  },
  { versionKey: false }
);

export const validate = (product: ProductSchema) => {
  return Joi.object({
    name: Joi.string().min(1).max(255).required(),
    description: Joi.string().min(1).required(),
    price: Joi.number().min(0.01).max(250_000).required(),
    stocks: Joi.number().min(1).max(999).required(),
    gameImage: Joi.string(),
  }).validate(product);
};

export const validateRequest = (req: ProductSchema) => {
  return Joi.object({
    name: Joi.string().min(1).max(255),
    description: Joi.string().min(1),
    price: Joi.number().min(0.01).max(250_000),
    stocks: Joi.number().min(0).max(999),
  }).validate(req);
};

export default model('Product', productSchema);
