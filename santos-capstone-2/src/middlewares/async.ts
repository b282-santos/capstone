import { Request, RequestHandler, Response } from 'express';

type RouteHandler = (req: Request, res: Response) => Promise<void>;

const asyncHandler = (handler: RouteHandler) => {
  const asyncFunction: RequestHandler = async (req, res, next) => {
    try {
      await handler(req, res);
    } catch (err) {
      next(err);
    }
  };

  return asyncFunction;
};

export default asyncHandler;
