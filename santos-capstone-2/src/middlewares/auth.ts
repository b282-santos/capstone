import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { Token } from '../models/User';

declare global {
  namespace Express {
    interface Request {
      user: Token;
    }
  }
}

const auth = (req: Request, res: Response, next: NextFunction) => {
  let token = req.headers.authorization;
  if (!token) return res.status(401).send('Access denied. No token provided.');

  try {
    token = token.slice(7, token.length);
    const decoded = jwt.verify(token, process.env.JWT_SECRET as string);
    req.user = decoded as Token;
    return next();
  } catch (err) {
    res.status(400).send('Invalid token.');
  }
};

export default auth;
