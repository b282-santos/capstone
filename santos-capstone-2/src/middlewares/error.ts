import { NextFunction, Request, Response } from 'express';

type ErrorMiddleware = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => void;

const error: ErrorMiddleware = (err, _req, res, _next) => {
  res.status(500).send({
    error: err.message,
    stack: process.env.NODE_ENV !== 'production' ? err.stack : null,
  });
};

export default error;
