import { NextFunction, Request, Response } from 'express';

const customer = (req: Request, res: Response, next: NextFunction) => {
  if (req.user.isAdmin)
    return res.status(403).send('Access denied. Customer account required.');

  return next();
};

export default customer;
