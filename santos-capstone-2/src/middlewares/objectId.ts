import { NextFunction, Request, Response } from 'express';
import { isValidObjectId } from 'mongoose';

const objectId = (req: Request, res: Response, next: NextFunction) => {
  if (!isValidObjectId(req.params.id))
    return res.status(400).send('Invalid ID.');

  return next();
};

export default objectId;
