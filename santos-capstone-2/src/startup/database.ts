import { Express } from 'express';
import { connect } from 'mongoose';

export default (app: Express) => {
  const database = process.env.MONGODB_URI as string;
  const port = process.env.PORT || 4000;

  connect(database)
    .then(() => {
      console.log('Connected to MongoDB');
      app.listen(port, () => console.log(`Listening on port ${port}...`));
    })
    .catch(err => console.log(err.message));
};
