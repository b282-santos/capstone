import cookieParser from 'cookie-parser';
import cors from 'cors';
import { Express, json } from 'express';
import 'express-async-errors';
import helmet from 'helmet';
import protect from '../middlewares/auth';
import customer from '../middlewares/customer';
import error from '../middlewares/error';
import auth from '../routes/auth';
import cart from '../routes/cart';
import orders from '../routes/orders';
import products from '../routes/products';
import users from '../routes/users';

export default (app: Express) => {
  app.use(cors());
  app.use(helmet());
  app.use(cookieParser());
  app.use(json());
  app.use('/users', users);
  app.use('/auth', auth);
  app.use('/products', products);
  app.use('/cart', [protect, customer], cart);
  app.use('/orders', protect, orders);
  app.use(error);
};
