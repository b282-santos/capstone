import 'dotenv/config';
import express from 'express';
import database from './startup/database';
import routes from './startup/routes';

const app = express();

database(app);
routes(app);
