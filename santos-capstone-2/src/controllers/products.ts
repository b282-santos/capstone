import { RequestHandler } from 'express';
import Product, { validate, validateRequest } from '../models/Product';

export const getAll: RequestHandler = async (_req, res) => {
  const products = await Product.find();

  res.send(products);
};

export const getAllActive: RequestHandler = async (req, res) => {
  const page = Number(req.query.page) || 1;
  const pageSize = Number(req.query.pageSize) || 2;
  const searchQuery = req.query.name || '';

  const filter: { name?: { $regex: RegExp } } = {};

  if (searchQuery !== '') {
    const regexPattern = new RegExp(searchQuery as string, 'i');
    filter.name = { $regex: regexPattern };
  }

  const totalItems = await Product.countDocuments({
    isActive: true,
    ...filter,
  });
  const totalPages = Math.ceil(totalItems / pageSize);

  const items = await Product.find({ isActive: true, ...filter })
    .skip((page - 1) * pageSize)
    .limit(pageSize);

  res.send({
    items,
    totalPages,
    currentPage: page,
  });
};

export const getOne: RequestHandler = async (req, res) => {
  const product = await Product.findById(req.params.id, '-createdOn');
  if (!product) return res.status(404).send('Product not found.');

  return res.send(product);
};

export const add: RequestHandler = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.message);

  const product = new Product(req.body);
  await product.save();

  return res.status(201).send(product);
};

export const update: RequestHandler = async (req, res) => {
  const { id } = req.params;

  const { error } = validateRequest(req.body);
  if (error) return res.status(400).send(error.message);

  let product = await Product.findById(id);
  if (!product) return res.status(404).send('Product not found.');

  product = await Product.findByIdAndUpdate(id, req.body, { new: true });
  return res.send(product);
};

export const activate: RequestHandler = async (req, res) => {
  const product = await Product.findById(req.params.id);
  if (!product) return res.status(404).send('Product not found.');

  product.isActive = !product.isActive;
  await product.save();

  return res.send(product);
};

export const deleteProduct: RequestHandler = async (req, res) => {
  await Product.findByIdAndDelete(req.params.id);
  return res.send({ message: 'Success' });
};
