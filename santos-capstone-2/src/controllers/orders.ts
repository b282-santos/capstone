import { RequestHandler } from 'express';
import Cart from '../models/Cart';
import Order from '../models/Order';
import Product from '../models/Product';

export const getAllOrders: RequestHandler = async (_req, res) => {
  const orders = await Order.find();
  if (!orders.length) return res.status(400).send('Order list is empty.');

  return res.send(orders);
};

export const checkOrders: RequestHandler = async (req, res) => {
  const orders = await Order.find({ userId: req.user._id });
  if (!orders.length) return res.status(400).send('Your order list is empty.');

  return res.send(orders);
};

export const placeOrder: RequestHandler = async (req, res) => {
  const cart = await Cart.findOne({ userId: req.user._id });
  if (!cart) return res.status(400).send('Your cart is empty.');

  const order = new Order({
    userId: req.user._id,
    name: req.user.name,
    address: req.user.address,
    phone: req.user.phone,
    products: cart.items,
    totalItems: cart.itemsInCart,
    totalAmount: cart.totalAmount,
  });

  for (const item of cart.items) {
    const product = await Product.findById(item.productId, 'stocks');
    if (!product) return;

    if (!product.stocks)
      return res.status(400).send(`${item.name} is out stock.`);

    if (product.stocks < item.quantity)
      return res
        .status(400)
        .send(
          `${item.name} stocks is insufficient. Remaining stocks is ${product.stocks}`
        );

    product.stocks -= item.quantity;
    await product.save();
  }

  await order.save();

  await Cart.findOneAndDelete({ userId: req.user._id });
  return res.send(order);
};
