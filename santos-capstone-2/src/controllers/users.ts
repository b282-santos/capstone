import { compare, genSalt, hash } from 'bcrypt';
import { RequestHandler } from 'express';
import _ from 'lodash';
import User, { validate } from '../models/User';

export const getAllCustomers: RequestHandler = async (_req, res) => {
  const users = await User.find({ isAdmin: false }, '-password -isAdmin');
  res.send(users);
};

export const getUserInfo: RequestHandler = async (req, res) => {
  const user = await User.findById(req.user._id, '-password');
  res.send(user);
};

export const registerUser: RequestHandler = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.message);

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send('Email address is already registered.');

  user = new User(req.body);
  const salt = await genSalt(10);
  user.password = await hash(req.body.password, salt);
  user.save();

  return res
    .status(201)
    .send(
      _.pick(user, ['_id', 'name', 'email', 'address', 'phone', 'isAdmin'])
    );
};

export const changePassword: RequestHandler = async (req, res) => {
  let user = await User.findById(req.user._id, 'password');
  if (!user) return;

  const validPassword = await compare(req.body.currentPassword, user.password);
  if (!validPassword)
    return res.status(400).send('Your current password is incorrect.');

  const salt = await genSalt(10);
  user.password = await hash(req.body.newPassword, salt);
  user.save();

  return res.send('Password successfully changed.');
};

export const setAdmin: RequestHandler = async (req, res) => {
  const user = await User.findById(req.params.id, '-password');
  if (!user) return res.status(404).send('User not found.');

  user.isAdmin = !user.isAdmin;
  user.save();

  return res.send(user);
};
