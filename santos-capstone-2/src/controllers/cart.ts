import { RequestHandler } from 'express';
import { isValidObjectId } from 'mongoose';
import Cart from '../models/Cart';
import Product from '../models/Product';

export const checkCart: RequestHandler = async (req, res) => {
  const cart = await Cart.findOne({ userId: req.user._id });
  if (!cart) return res.status(200).send('Your cart is empty.');

  cart.computeTotalAmount();
  await cart.save();

  return res.send(cart);
};

export const addToCart: RequestHandler = async (req, res) => {
  const { productId } = req.body;
  const quantity = req.body.quantity || 1;

  if (!isValidObjectId(productId))
    return res.status(400).send('Invalid product ID.');

  const product = await Product.findById(productId);
  if (!product || !product.isActive || !product.stocks)
    return res.status(404).send('Invalid or unavailable product.');

  const { _id, name, price, stocks, gameImage } = product;
  if (stocks < quantity) return res.status(400).send('Insufficient stocks.');

  let cart = await Cart.findOne({ userId: req.user._id });
  if (!cart) cart = new Cart({ userId: req.user._id, items: [] });

  const item = cart.items.find(item => `${item.productId}` === `${_id}`);
  if (item) {
    item.quantity += quantity;
    item.subTotal = item.quantity * item.price;
  } else {
    cart.items.push({
      ...req.body,
      name,
      price,
      quantity,
      subTotal: price * quantity,
      gameImage,
    });
  }

  cart.countItemsInCart();
  cart.computeTotalAmount();
  await cart.save();

  return res.send(cart);
};

export const updateQuantity: RequestHandler = async (req, res) => {
  // const { error } = validateQuantity(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  let cart = await Cart.findOne({ userId: req.user._id });
  if (!cart) return res.status(404).send('Cart not found.');

  const item = cart.items.find(item => `${item.productId}` === req.params.id);
  if (!item) return res.status(404).send('Item not found.');

  const product = await Product.findById(req.params.id, 'stocks');
  if (!product) return res.status(404).send('Product not found.');

  if (product.stocks < req.body.quantity)
    return res.status(400).send('Insufficient stocks.');

  if (req.body.quantity === 0) {
    const index = cart.items.indexOf(item);
    cart.items.splice(index, 1);
  } else {
    item.quantity = req.body.quantity;
    item.subTotal = item.quantity * item.price;
  }

  cart.countItemsInCart();
  cart.computeTotalAmount();
  await cart.save();

  if (!cart.items.length) {
    await Cart.deleteOne({ userId: req.user._id });
    return res.status(200).send('Your cart is empty.');
  }

  return res.send(cart);
};

export const decreaseQuantity: RequestHandler = async (req, res) => {
  let cart = await Cart.findOne({ userId: req.user._id });
  if (!cart) return res.status(404).send('Cart not found.');

  const item = cart.items.find(item => `${item.productId}` === req.params.id);
  if (!item) return res.status(404).send('Item not found.');

  if (item.quantity === 1) {
    const index = cart.items.indexOf(item);
    cart.items.splice(index, 1);
  } else {
    item.quantity--;
    item.subTotal = item.quantity * item.price;
  }

  cart.countItemsInCart();
  cart.computeTotalAmount();
  await cart.save();

  if (!cart.items.length) {
    await Cart.deleteOne({ userId: req.user._id });
    return res.status(400).send('Your cart is empty.');
  }

  return res.send(cart);
};

export const removeAll: RequestHandler = async (req, res) => {
  let cart = await Cart.findOne({ userId: req.user._id });
  if (!cart) return res.status(404).send('Cart not found.');

  await Cart.deleteOne({ userId: req.user._id });
  return res.status(200).send('Your cart is empty.');
};

export const removeItem: RequestHandler = async (req, res) => {
  let cart = await Cart.findOne({ userId: req.user._id });
  if (!cart) return res.status(404).send('Cart not found.');

  const item = cart.items.find(item => `${item.productId}` === req.params.id);
  if (!item) return res.status(404).send('Item not found.');

  const index = cart.items.indexOf(item);
  cart.items.splice(index, 1);
  cart.countItemsInCart();
  cart.computeTotalAmount();
  await cart.save();

  if (!cart.items.length) {
    await Cart.deleteOne({ userId: req.user._id });
    return res.status(200).send('Your cart is empty.');
  }

  return res.send(cart);
};
