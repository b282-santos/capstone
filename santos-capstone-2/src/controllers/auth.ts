import { compare } from 'bcrypt';
import { RequestHandler } from 'express';
import User from '../models/User';

export const authenticateUser: RequestHandler = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send('Invalid email or password.');

  const validPassword = await compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).send('Invalid email or password.');

  const token = user.generateAuthToken();
  return res.send(token);
};
