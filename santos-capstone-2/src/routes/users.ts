import { Router } from 'express';
import * as controller from '../controllers/users';
import admin from '../middlewares/admin';
import auth from '../middlewares/auth';
import objectId from '../middlewares/objectId';

const router = Router();

router.get('/', [auth, admin], controller.getAllCustomers);
router.get('/me', auth, controller.getUserInfo);
router.post('/', controller.registerUser);
router.patch('/me/password', auth, controller.changePassword);
router.patch('/:id/admin', [auth, admin, objectId], controller.setAdmin);

export default router;
