import { Router } from 'express';
import * as controller from '../controllers/orders';
import admin from '../middlewares/admin';
import customer from '../middlewares/customer';

const router = Router();

router.get('/', admin, controller.getAllOrders);
router.get('/me', customer, controller.checkOrders);
router.post('/', customer, controller.placeOrder);

export default router;
