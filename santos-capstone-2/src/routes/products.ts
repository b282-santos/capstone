import { Router } from 'express';
import * as controller from '../controllers/products';
import admin from '../middlewares/admin';
import auth from '../middlewares/auth';
import objectId from '../middlewares/objectId';

const router = Router();

router.get('/', [auth, admin], controller.getAll);
router.get('/active', controller.getAllActive);
router.get('/:id', objectId, controller.getOne);
router.post('/', [auth, admin], controller.add);
router.put('/:id', [auth, admin, objectId], controller.update);
router.patch('/:id/status', [auth, admin, objectId], controller.activate);
router.delete('/:id', [auth, admin, objectId], controller.deleteProduct);

export default router;
