import { Router } from 'express';
import * as controller from '../controllers/cart';
import objectId from '../middlewares/objectId';

const router = Router();

router.get('/items', controller.checkCart);
router.post('/items', controller.addToCart);
router.patch('/items/:id/quantity', objectId, controller.updateQuantity);
router.patch('/items/:id', objectId, controller.decreaseQuantity);
router.delete('/items/all', controller.removeAll);
router.delete('/items/:id', objectId, controller.removeItem);

export default router;
