"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const auth = (req, res, next) => {
    let token = req.headers.authorization;
    if (!token)
        return res.status(401).send('Access denied. No token provided.');
    try {
        token = token.slice(7, token.length);
        const decoded = jsonwebtoken_1.default.verify(token, process.env.JWT_SECRET);
        req.user = decoded;
        return next();
    }
    catch (err) {
        res.status(400).send('Invalid token.');
    }
};
exports.default = auth;
