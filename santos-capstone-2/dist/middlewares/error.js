"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const error = (err, _req, res, _next) => {
    res.status(500).send({
        error: err.message,
        stack: process.env.NODE_ENV !== 'production' ? err.stack : null,
    });
};
exports.default = error;
