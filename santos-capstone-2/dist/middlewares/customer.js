"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const customer = (req, res, next) => {
    if (req.user.isAdmin)
        return res.status(403).send('Access denied. Customer account required.');
    return next();
};
exports.default = customer;
