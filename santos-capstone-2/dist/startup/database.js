"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.default = (app) => {
    const database = process.env.MONGODB_URI;
    const port = process.env.PORT || 4000;
    (0, mongoose_1.connect)(database)
        .then(() => {
        console.log('Connected to MongoDB');
        app.listen(port, () => console.log(`Listening on port ${port}...`));
    })
        .catch(err => console.log(err.message));
};
