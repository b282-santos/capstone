"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateRequest = exports.validate = void 0;
const joi_1 = __importDefault(require("joi"));
const mongoose_1 = require("mongoose");
const productSchema = new mongoose_1.Schema({
    name: {
        type: String,
        min: 1,
        max: 255,
        trim: true,
        set: (value) => value.replace(/\s+/g, ' '),
        required: true,
    },
    description: {
        type: String,
        min: 1,
        required: true,
    },
    price: {
        type: Number,
        min: 0.01,
        max: 250000,
        required: true,
    },
    stocks: {
        type: Number,
        min: 0,
        max: 999,
        required: true,
    },
    gameImage: String,
    isActive: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: new Date(),
    },
}, { versionKey: false });
const validate = (product) => {
    return joi_1.default.object({
        name: joi_1.default.string().min(1).max(255).required(),
        description: joi_1.default.string().min(1).required(),
        price: joi_1.default.number().min(0.01).max(250000).required(),
        stocks: joi_1.default.number().min(1).max(999).required(),
        gameImage: joi_1.default.string(),
    }).validate(product);
};
exports.validate = validate;
const validateRequest = (req) => {
    return joi_1.default.object({
        name: joi_1.default.string().min(1).max(255),
        description: joi_1.default.string().min(1),
        price: joi_1.default.number().min(0.01).max(250000),
        stocks: joi_1.default.number().min(0).max(999),
    }).validate(req);
};
exports.validateRequest = validateRequest;
exports.default = (0, mongoose_1.model)('Product', productSchema);
