"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validate = void 0;
const joi_1 = __importDefault(require("joi"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const mongoose_1 = require("mongoose");
const userSchema = new mongoose_1.Schema({
    name: {
        type: String,
        min: 1,
        max: 50,
        trim: true,
        set: (value) => value.replace(/\s+/g, ' '),
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        min: 6,
        max: 1024,
        required: true,
    },
    address: {
        type: String,
        min: 10,
        max: 100,
        trim: true,
        set: (value) => value.replace(/\s+/g, ' '),
        required: true,
    },
    phone: {
        type: String,
        min: 11,
        max: 11,
        trim: true,
        set: (value) => value.replace(/\s+/g, ''),
        required: true,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
}, { versionKey: false });
userSchema.methods.generateAuthToken = function () {
    return jsonwebtoken_1.default.sign({
        _id: this._id,
        name: this.name,
        address: this.address,
        phone: this.phone,
        isAdmin: this.isAdmin,
    }, process.env.JWT_SECRET);
};
const User = (0, mongoose_1.model)('User', userSchema);
exports.default = User;
const validate = (user) => {
    return joi_1.default.object({
        name: joi_1.default.string().min(1).max(50).required(),
        email: joi_1.default.string().email().required(),
        password: joi_1.default.string().min(6).max(30).required(),
        address: joi_1.default.string().min(10).max(100).required(),
        phone: joi_1.default.string().min(11).max(11).required(),
    }).validate(user);
};
exports.validate = validate;
