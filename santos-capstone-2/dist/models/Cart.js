"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import Joi from 'joi';
const mongoose_1 = require("mongoose");
const cartSchema = new mongoose_1.Schema({
    userId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    items: [
        {
            productId: {
                type: mongoose_1.Schema.Types.ObjectId,
                ref: 'Product',
                required: true,
            },
            name: {
                type: String,
                required: true,
            },
            price: {
                type: Number,
                required: true,
            },
            quantity: {
                type: Number,
                required: true,
            },
            subTotal: {
                type: Number,
                required: true,
            },
            gameImage: String,
        },
    ],
    itemsInCart: {
        type: Number,
        default: 0,
    },
    totalAmount: {
        type: Number,
        default: 0,
    },
}, { versionKey: false });
cartSchema.methods.countItemsInCart = function () {
    this.itemsInCart = this.items.length;
};
cartSchema.methods.computeTotalAmount = function () {
    this.totalAmount = this.items.reduce((total, item) => total + item.subTotal, 0);
};
const Cart = (0, mongoose_1.model)('Cart', cartSchema);
exports.default = Cart;
// export const validate = req => {
//   const schema = Joi.object({
//     productId: Joi.required(),
//     quantity: Joi.number().min(1).max(999),
//   });
//   return schema.validate(req);
// };
// export const validateQuantity = req => {
//   const schema = Joi.object({
//     quantity: Joi.number().min(0).max(999).required(),
//   });
//   return schema.validate(req);
// };
