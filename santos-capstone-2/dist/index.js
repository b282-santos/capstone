"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const express_1 = __importDefault(require("express"));
const database_1 = __importDefault(require("./startup/database"));
const routes_1 = __importDefault(require("./startup/routes"));
const app = (0, express_1.default)();
(0, database_1.default)(app);
(0, routes_1.default)(app);
