"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeItem = exports.removeAll = exports.decreaseQuantity = exports.updateQuantity = exports.addToCart = exports.checkCart = void 0;
const mongoose_1 = require("mongoose");
const Cart_1 = __importDefault(require("../models/Cart"));
const Product_1 = __importDefault(require("../models/Product"));
const checkCart = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const cart = yield Cart_1.default.findOne({ userId: req.user._id });
    if (!cart)
        return res.status(200).send('Your cart is empty.');
    cart.computeTotalAmount();
    yield cart.save();
    return res.send(cart);
});
exports.checkCart = checkCart;
const addToCart = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { productId } = req.body;
    const quantity = req.body.quantity || 1;
    if (!(0, mongoose_1.isValidObjectId)(productId))
        return res.status(400).send('Invalid product ID.');
    const product = yield Product_1.default.findById(productId);
    if (!product || !product.isActive || !product.stocks)
        return res.status(404).send('Invalid or unavailable product.');
    const { _id, name, price, stocks, gameImage } = product;
    if (stocks < quantity)
        return res.status(400).send('Insufficient stocks.');
    let cart = yield Cart_1.default.findOne({ userId: req.user._id });
    if (!cart)
        cart = new Cart_1.default({ userId: req.user._id, items: [] });
    const item = cart.items.find(item => `${item.productId}` === `${_id}`);
    if (item) {
        item.quantity += quantity;
        item.subTotal = item.quantity * item.price;
    }
    else {
        cart.items.push(Object.assign(Object.assign({}, req.body), { name,
            price,
            quantity, subTotal: price * quantity, gameImage }));
    }
    cart.countItemsInCart();
    cart.computeTotalAmount();
    yield cart.save();
    return res.send(cart);
});
exports.addToCart = addToCart;
const updateQuantity = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // const { error } = validateQuantity(req.body);
    // if (error) return res.status(400).send(error.details[0].message);
    let cart = yield Cart_1.default.findOne({ userId: req.user._id });
    if (!cart)
        return res.status(404).send('Cart not found.');
    const item = cart.items.find(item => `${item.productId}` === req.params.id);
    if (!item)
        return res.status(404).send('Item not found.');
    const product = yield Product_1.default.findById(req.params.id, 'stocks');
    if (!product)
        return res.status(404).send('Product not found.');
    if (product.stocks < req.body.quantity)
        return res.status(400).send('Insufficient stocks.');
    if (req.body.quantity === 0) {
        const index = cart.items.indexOf(item);
        cart.items.splice(index, 1);
    }
    else {
        item.quantity = req.body.quantity;
        item.subTotal = item.quantity * item.price;
    }
    cart.countItemsInCart();
    cart.computeTotalAmount();
    yield cart.save();
    if (!cart.items.length) {
        yield Cart_1.default.deleteOne({ userId: req.user._id });
        return res.status(200).send('Your cart is empty.');
    }
    return res.send(cart);
});
exports.updateQuantity = updateQuantity;
const decreaseQuantity = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let cart = yield Cart_1.default.findOne({ userId: req.user._id });
    if (!cart)
        return res.status(404).send('Cart not found.');
    const item = cart.items.find(item => `${item.productId}` === req.params.id);
    if (!item)
        return res.status(404).send('Item not found.');
    if (item.quantity === 1) {
        const index = cart.items.indexOf(item);
        cart.items.splice(index, 1);
    }
    else {
        item.quantity--;
        item.subTotal = item.quantity * item.price;
    }
    cart.countItemsInCart();
    cart.computeTotalAmount();
    yield cart.save();
    if (!cart.items.length) {
        yield Cart_1.default.deleteOne({ userId: req.user._id });
        return res.status(400).send('Your cart is empty.');
    }
    return res.send(cart);
});
exports.decreaseQuantity = decreaseQuantity;
const removeAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let cart = yield Cart_1.default.findOne({ userId: req.user._id });
    if (!cart)
        return res.status(404).send('Cart not found.');
    yield Cart_1.default.deleteOne({ userId: req.user._id });
    return res.status(200).send('Your cart is empty.');
});
exports.removeAll = removeAll;
const removeItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let cart = yield Cart_1.default.findOne({ userId: req.user._id });
    if (!cart)
        return res.status(404).send('Cart not found.');
    const item = cart.items.find(item => `${item.productId}` === req.params.id);
    if (!item)
        return res.status(404).send('Item not found.');
    const index = cart.items.indexOf(item);
    cart.items.splice(index, 1);
    cart.countItemsInCart();
    cart.computeTotalAmount();
    yield cart.save();
    if (!cart.items.length) {
        yield Cart_1.default.deleteOne({ userId: req.user._id });
        return res.status(200).send('Your cart is empty.');
    }
    return res.send(cart);
});
exports.removeItem = removeItem;
