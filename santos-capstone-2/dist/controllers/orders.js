"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.placeOrder = exports.checkOrders = exports.getAllOrders = void 0;
const Cart_1 = __importDefault(require("../models/Cart"));
const Order_1 = __importDefault(require("../models/Order"));
const Product_1 = __importDefault(require("../models/Product"));
const getAllOrders = (_req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const orders = yield Order_1.default.find();
    if (!orders.length)
        return res.status(400).send('Order list is empty.');
    return res.send(orders);
});
exports.getAllOrders = getAllOrders;
const checkOrders = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const orders = yield Order_1.default.find({ userId: req.user._id });
    if (!orders.length)
        return res.status(400).send('Your order list is empty.');
    return res.send(orders);
});
exports.checkOrders = checkOrders;
const placeOrder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const cart = yield Cart_1.default.findOne({ userId: req.user._id });
    if (!cart)
        return res.status(400).send('Your cart is empty.');
    const order = new Order_1.default({
        userId: req.user._id,
        name: req.user.name,
        address: req.user.address,
        phone: req.user.phone,
        products: cart.items,
        totalItems: cart.itemsInCart,
        totalAmount: cart.totalAmount,
    });
    for (const item of cart.items) {
        const product = yield Product_1.default.findById(item.productId, 'stocks');
        if (!product)
            return;
        if (!product.stocks)
            return res.status(400).send(`${item.name} is out stock.`);
        if (product.stocks < item.quantity)
            return res
                .status(400)
                .send(`${item.name} stocks is insufficient. Remaining stocks is ${product.stocks}`);
        product.stocks -= item.quantity;
        yield product.save();
    }
    yield order.save();
    yield Cart_1.default.findOneAndDelete({ userId: req.user._id });
    return res.send(order);
});
exports.placeOrder = placeOrder;
