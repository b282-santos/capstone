"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setAdmin = exports.changePassword = exports.registerUser = exports.getUserInfo = exports.getAllCustomers = void 0;
const bcrypt_1 = require("bcrypt");
const lodash_1 = __importDefault(require("lodash"));
const User_1 = __importStar(require("../models/User"));
const getAllCustomers = (_req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const users = yield User_1.default.find({ isAdmin: false }, '-password -isAdmin');
    res.send(users);
});
exports.getAllCustomers = getAllCustomers;
const getUserInfo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User_1.default.findById(req.user._id, '-password');
    res.send(user);
});
exports.getUserInfo = getUserInfo;
const registerUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error } = (0, User_1.validate)(req.body);
    if (error)
        return res.status(400).send(error.message);
    let user = yield User_1.default.findOne({ email: req.body.email });
    if (user)
        return res.status(400).send('Email address is already registered.');
    user = new User_1.default(req.body);
    const salt = yield (0, bcrypt_1.genSalt)(10);
    user.password = yield (0, bcrypt_1.hash)(req.body.password, salt);
    user.save();
    return res
        .status(201)
        .send(lodash_1.default.pick(user, ['_id', 'name', 'email', 'address', 'phone', 'isAdmin']));
});
exports.registerUser = registerUser;
const changePassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let user = yield User_1.default.findById(req.user._id, 'password');
    if (!user)
        return;
    const validPassword = yield (0, bcrypt_1.compare)(req.body.currentPassword, user.password);
    if (!validPassword)
        return res.status(400).send('Your current password is incorrect.');
    const salt = yield (0, bcrypt_1.genSalt)(10);
    user.password = yield (0, bcrypt_1.hash)(req.body.newPassword, salt);
    user.save();
    return res.send('Password successfully changed.');
});
exports.changePassword = changePassword;
const setAdmin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield User_1.default.findById(req.params.id, '-password');
    if (!user)
        return res.status(404).send('User not found.');
    user.isAdmin = !user.isAdmin;
    user.save();
    return res.send(user);
});
exports.setAdmin = setAdmin;
