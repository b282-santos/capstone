"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteProduct = exports.activate = exports.update = exports.add = exports.getOne = exports.getAllActive = exports.getAll = void 0;
const Product_1 = __importStar(require("../models/Product"));
const getAll = (_req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const products = yield Product_1.default.find();
    res.send(products);
});
exports.getAll = getAll;
const getAllActive = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const page = Number(req.query.page) || 1;
    const pageSize = Number(req.query.pageSize) || 2;
    const searchQuery = req.query.name || '';
    const filter = {};
    if (searchQuery !== '') {
        const regexPattern = new RegExp(searchQuery, 'i');
        filter.name = { $regex: regexPattern };
    }
    const totalItems = yield Product_1.default.countDocuments(Object.assign({ isActive: true }, filter));
    const totalPages = Math.ceil(totalItems / pageSize);
    const items = yield Product_1.default.find(Object.assign({ isActive: true }, filter))
        .skip((page - 1) * pageSize)
        .limit(pageSize);
    res.send({
        items,
        totalPages,
        currentPage: page,
    });
});
exports.getAllActive = getAllActive;
const getOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const product = yield Product_1.default.findById(req.params.id, '-createdOn');
    if (!product)
        return res.status(404).send('Product not found.');
    return res.send(product);
});
exports.getOne = getOne;
const add = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error } = (0, Product_1.validate)(req.body);
    if (error)
        return res.status(400).send(error.message);
    const product = new Product_1.default(req.body);
    yield product.save();
    return res.status(201).send(product);
});
exports.add = add;
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { error } = (0, Product_1.validateRequest)(req.body);
    if (error)
        return res.status(400).send(error.message);
    let product = yield Product_1.default.findById(id);
    if (!product)
        return res.status(404).send('Product not found.');
    product = yield Product_1.default.findByIdAndUpdate(id, req.body, { new: true });
    return res.send(product);
});
exports.update = update;
const activate = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const product = yield Product_1.default.findById(req.params.id);
    if (!product)
        return res.status(404).send('Product not found.');
    product.isActive = !product.isActive;
    yield product.save();
    return res.send(product);
});
exports.activate = activate;
const deleteProduct = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    yield Product_1.default.findByIdAndDelete(req.params.id);
    return res.send({ message: 'Success' });
});
exports.deleteProduct = deleteProduct;
